import Vuex from 'vuex';

// Components
import LoadingComponent from './components/loading';
import LoadingSpin from './components/loading-spin';
import Modal from './components/modal';
import Paginator from './components/paginator';
import CanSetPermission from './components/canSetPermission';
import DuploSelect from './components/duploselect';
import DuploSelectRest from './components/rest/duploeselectrest';
import SelectRest from './components/rest/selectrest';
import DropDown from './components/dropdown';
import DropDownSwitch from './components/dropdownswitch';
import FormGroup from './components/form/formgroup';
import DatePicker from './components/datepicker';
import DatePickerForm from './components/form/datepickerform';
import Tab from './components/tabs/tab';
import Tabs from './components/tabs/tabs';
import NextTab from './components/tabs/nextTab';
import PrevTab from './components/tabs/prevTab';
import InputBoxModel from './components/form/inputboxmodel';
import RestoreForm from './components/restoreform';
import FormContainer from './components/form/formcontainer';
import CheckBoxGroup from './components/checkboxgroup';
import ListGroupRest from './components/rest/listgrouprest';
import FileUpload from './components/fileupload';
import BoxCard from './components/box-card';

// Diretivas
import Can from './directives/can'
import JsonContent from './directives/jsoncontent'
import ClickOutside from './directives/clickoutside'

// Mixins
import DataTablesMixin from './mixins/datatables.mixin';

import VueTheMask from 'vue-the-mask';

// Packages
import Alert from './packages/Alert';
import Convert from './packages/Convert';
import Generator from './packages/Generator';
import Request from './packages/Request';


// Instal the components
function install (Vue) {
    // Vue.component(TheMask.name, TheMask)
    // Registrando os componentes
    Vue.canController = Can.canController;

    // Packages
    Vue.Alert = Alert.pack;
    Vue.Convert = Convert.pack;
    Vue.Generator = Generator.pack;
    Vue.Request = Request.pack;

    Vue.Alert.init();

    // Componentes
    Vue.component(LoadingComponent.name, LoadingComponent);
    Vue.component(LoadingSpin.name, LoadingSpin);
    Vue.component(Modal.name, Modal);
	Vue.component(Paginator.name, Paginator);
	Vue.component(CanSetPermission.name, CanSetPermission);
	Vue.component(DuploSelect.name, DuploSelect);
	Vue.component(DuploSelectRest.name, DuploSelectRest);
	Vue.component(SelectRest.name, SelectRest);
	Vue.component(DropDown.name, DropDown);
	Vue.component(DropDownSwitch.name, DropDownSwitch);
	Vue.component(FormGroup.name, FormGroup);
	Vue.component(DatePicker.name, DatePicker);
    Vue.component(InputBoxModel.name, InputBoxModel);
    Vue.component(InputBoxModel.name, InputBoxModel);
    Vue.component(RestoreForm.name, RestoreForm);
    Vue.component(FormContainer.name, FormContainer);
    Vue.component(DatePickerForm.name, DatePickerForm);
    Vue.component(CheckBoxGroup.name, CheckBoxGroup);
    Vue.component(ListGroupRest.name, ListGroupRest);
    Vue.component(FileUpload.name, FileUpload);
    Vue.component(BoxCard.name, BoxCard);
    //Tabs Components
	Vue.component(Tab.name, Tab);
	Vue.component(Tabs.name, Tabs);
	Vue.component(NextTab.name, NextTab);
	Vue.component(PrevTab.name, PrevTab);

	// Directives
    Vue.directive(Can.name, Can.directive);
    Vue.directive(JsonContent.name, JsonContent.directive);
    Vue.directive(ClickOutside.name, ClickOutside.directive);

    // Thirds
    Vue.use(VueTheMask);

    Vue.use(Vuex);
}




export default install;
export {
    LoadingComponent,
    Modal,
    Paginator,
    CanSetPermission,
    DuploSelect,
    DuploSelectRest,
    SelectRest,
    DataTablesMixin,
    JsonContent
};

// Install by default if included from script tag
if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(install);

}