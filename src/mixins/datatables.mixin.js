window.pdfMake = require('pdfmake/build/pdfmake.min.js');
let pdfFonts = require('pdfmake/build/vfs_fonts.js');
window.pdfMake.vfs = pdfFonts.pdfMake.vfs;


import DropDown from '../components/dropdown';
import DropDownSwitch from '../components/dropdownswitch';


export default {
    data: function () {
        let default_data = require('./defaults.json');
        let DropDownComponent = Vue.extend(DropDown);
        let DropDownSwitchComponent = Vue.extend(DropDownSwitch);

        return {
            useDefaults: true,
            order: [[ 0, "desc" ]],
            classButton: 'btn btn-outline-primary dt-button',
            image: default_data.image,
            filtroAtual: {},
            loading: false,
            Datatables: {},
            buttons: {},
            dataTableButtons: [],
            DropDownComponent: DropDownComponent,
            DropDownSwitchComponent: DropDownSwitchComponent,
            urlTable: '',
            method: 'GET',
            header: {},
            actions: {},
            roleActions: {},
            idObject: 'datatables_',
            newAction: function () {
            }
        }
    },
    mounted: function () {
        let self = this;

        Vue.nextTick(function() {
            self.createDataTables();
        });
    },
    methods: {
        recreateDataTables: function(){
            let self = this;
            self.destroyDataTables();
            let parent = $('#'+self.idObject).parent().get(0);
            let actual = $('#'+self.idObject).get(0);
            let newElement = document.createElement("DIV");
            newElement.id = self.idObject;
            parent.replaceChild(actual, newElement);

            self.createDataTables();
        },
        destroyDataTables: function(){
            let self = this;
            $('#' + self.idObject).DataTable().clear().destroy();
        },
        createDataTables: function(){
            let self = this;
            let fields = [];
            let listDropDownColumn = {};
            let listDropDownExport = {};

            let ColumnsDefs = [];
            let j = 0, i = 0;

            for (j in self.actions) {
                let title = self.actions[j].name;
                listDropDownColumn[i] = {title: self.actions[j].title};
                if (typeof self.actions[j].formatter !== "function") {
                    self.actions[j].formatter = function (data) {
                        return data;
                    }
                }

                fields.push(
                    {
                        title: self.actions[j].title,
                        name: self.actions[j].name,
                        "defaultContent": self.actions[j].content,
                        data: 0,
                        render: self.actions[j].formatter,
                        "searchable": false
                    }
                );

                ColumnsDefs.push(
                    {
                        "targets": i++,
                        "data": null,
                        "orderable": false,
                        "defaultContent": self.actions[j].content,
                        createdCell: function (td, cellData, rowData, row, col) {
                            $(td).attr('data-title', title);
                        }
                    }
                );
            }

            for (j in self.header) {
                let title = self.header[j].title;
                listDropDownColumn[i] = {title: self.header[j].title};
                if (typeof self.header[j].formatter !== "function") {
                    self.header[j].formatter = function (data) {
                        return data;
                    }
                }
                fields[i++] =
                    {

                        title: self.header[j].title,
                        name: self.header[j].name,
                        data: self.header[j].data,
                        render: self.header[j].formatter,
                        createdCell: function (td, cellData, rowData, row, col) {
                            $(td).attr('data-title', title);
                        }
                    };
            }

            let DropDownConfigurarGrid = new self.DropDownSwitchComponent(
                {
                    propsData: {
                        list: listDropDownColumn,
                        title: '<i class="fa fa-th" aria-hidden="true"></i> Configurar Colunas',
                        classButton: self.classButton
                    },
                    created: function () {
                        this.$on('dropdownClicked', function (props) {
                            self.columnDropdownEvent(props);
                        });
                    }
                }
            );

            DropDownConfigurarGrid.$mount();

            listDropDownExport = {
                0: {idButton: self.buttons['excel'], title: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;Excel'},
                1: {idButton: self.buttons['csv'], title: '<i class="fa fa-table" aria-hidden="true"></i>&nbsp;CSV'}
            };

            let DropDownExport = new self.DropDownComponent(
                {
                    propsData: {
                        list: listDropDownExport,
                        title: '<i class="fa fa-download" aria-hidden="true"></i> Exportar',
                        classButton: self.classButton
                    },
                    created: function () {
                        this.$on('dropdownClicked', function (props) {
                            self.Datatables.button(props.idButton).trigger();
                        });
                    }
                }
            );

            DropDownExport.$mount();

            $.fn.dataTable.ext.errMode = 'none';

            let filtroCollpase = document.createElement("DIV");
            self.filtroAtual = $('.filtro', self.$el).get(0);

            $(self.filtroAtual).css("display", "none");
            self.$el.replaceChild(filtroCollpase, self.filtroAtual);
            $(filtroCollpase).append(self.filtroAtual);

            self.Datatables = $('#' + self.idObject).on('error.dt', function (e, settings, techNote, message) {
                console.log('An error has been reported by DataTables: ', message);
            }).DataTable({
                "language": {
                    "url": "/datatables-lang/portuguese-br.json"
                },
                destroy: true,
                processing: false,
                serverSide: true,
                paging: true,
                bFilter: false,
                ordering: true,
                searching: true,
                scrollX: false,
                bStateSave: false,
                "order": self.order,
                ajax: {
                    "url": self.urlTable,
                    "type": self.method,
                    beforeSend: function (request) {
                        self.loading = true;
                        let apiToken = document.head.querySelector('meta[name="api-token"]');
                        request.setRequestHeader("Authorization", 'Bearer ' + apiToken.content);
                    },
                    "data": function (d) {
                        let custom_request = {};
                        custom_request['add_query'] = {};
                        $('.filtro input[role="query"][type="text"], ' +
                            '.filtro input:checked[role="query"][type="radio"]', self.$el).each(function () {
                            custom_request['add_query'][$(this).data("column")] = $(this).val();
                        });

                        return $.extend({}, d, custom_request);
                    }
                },
                "fnDrawCallback": function (data) {
                    self.loading = false;

                    $(DropDownConfigurarGrid.$el).css("display", "inline").css("margin-left", "5px");
                    $(DropDownExport.$el).css("display", "inline").css("margin-left", "5px");
                    $('.drodown-columns', self.$el).replaceWith(DropDownConfigurarGrid.$el);
                    $('.btn-export', self.$el).replaceWith(DropDownExport.$el);
                    $('#' + self.idObject).css("width", "100%");
                },
                initComplete: function () {
                    $(this).wrap('<div class="table-container" />').removeClass("d-none");
                },
                "buttons": self.dataTableButtons,
                "aLengthMenu": [[10, 25, -1], [10, 25, "Todos"]],
                sDom: '<"top pull-right mb-2"B><"top"><"mb-2"rt><"bottom pull-left"l><"bottom pull-right"p><"bottom text-center"i>',
                dom: 't',
                //dom: 'Bfrtip',

                columns: fields,
                ColumnsDefs: ColumnsDefs,
                "rowCallback": function (row, data) {
                    for (let j in data) {
                        $(row).attr('data-column-' + j, data[j]);
                    }

                }
            });

            $('#' + self.idObject).on('click', 'button', function () {

                self.actionClick(this, $(this).closest('tr').data(), $(this).attr('role'));
            });
        },
        columnVisibilty: function (idx, value) {
            let self = this;
            self.Datatables.columns(idx).visible(value);
            $('#' + self.idObject).css("width", "100%");
        },
        refreshDataTable: function () {
            let self = this;
            self.Datatables.draw(false);
        },
        exportCSV: function () {
            let self = this;
            self.Datatables.button(3).trigger();
            // self.Datatables.button(0).trigger();
        },
        exportExcel: function () {
            let self = this;
            self.Datatables.button(3).trigger();
            // self.Datatables.button(0).trigger();
        },
        exportPDF: function () {
            let self = this;
            self.Datatables.button(4).trigger();
            // self.Datatables.button(0).trigger();
        },
        actionClick: function (obj, props, role) {
            if (typeof role !== "undefined") {
                if (typeof this.roleActions[role] === "function") {
                    this.roleActions[role](obj, props);
                }

                this.$emit(
                    role,
                    obj,
                    props
                );
            }
        },
        Pesquisar: function () {
            let self = this;

            $('.filtro input[role="datatables"], .filtro select[role="datatables"]', this.$el).each(function () {
                let column = $(this).data("column");
                let value = $(this).val();

                self.Datatables.columns(column).search(value);
            });

            self.refreshDataTable();
        },
        columnDropdownEvent: function (props) {
            this.columnVisibilty(props.index, props.value);
        },
        addExportButton: function () {

            this.addButton('export',
                {
                    text: 'exportar',
                    className: 'btn-export d-none'
                }
            );
            this.addButton('copy',
                {
                    text: '<i class="fa fa-clipboard" aria-hidden="true"></i>',
                    extend: 'copy',
                    exportOptions: {
                        columns: [':visible']
                    },
                    className: 'd-none'
                }
            );
            this.addButton('csv',
                {
                    text: '<i class="fa fa-table" aria-hidden="true"></i>',
                    extend: 'csv',
                    exportOptions: {
                        columns: [':visible']
                    },
                    className: 'd-none'
                }
            );
            this.addButton('excel',
                {
                    text: '<i class="fa fa-file-excel-o"></i>',
                    extend: 'excel',
                    exportOptions: {
                        columns: [':visible']
                    },
                    className: 'd-none'
                }
            );

            this.addButton('print',
                {
                    text: '<i class="fa fa-print hide"></i>',
                    extend: 'print',
                    exportOptions: {
                        columns: [':visible']
                    },
                    className: 'd-none'
                }
            );

            this.addButton('pdf',
                {
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    extend: 'pdfHtml5',
                    className: 'd-none',
                    orientation: 'landscape',
                    title: '',
                    exportOptions: {
                        columns: [':visible']
                    },
                    customize: function (doc) {
                        // Splice the image in after the header, but before the table
                        doc.content.splice(0, 0, {
                            margin: [0, 0, 0, 12],
                            alignment: 'center',
                            image: self.image
                        });

                        // Data URL generated by http://dataurl.net/#dataurlmaker
                    }
                }
            );
        },
        addConfigurarButton: function () {
            this.addButton('configurar', {
                text: 'drodown-columns',
                className: 'drodown-columns d-none'
            });
        },
        addRefreshButton: function () {
            let self = this;

            this.addButton('refresh', {
                text: '<i class="fa fa-refresh" aria-hidden="true"></i> Atualizar',
                action: function (e, dt, node, config) {
                    self.refreshDataTable();
                },
                className: 'btn btn-outline-primary'
            });
        },
        addShowFilterButton: function () {
            let self = this;

            this.addButton('filter', {
                text: '<i class="fa fa-filter" aria-hidden="true"></i> Filtros',
                action: function (e, dt, node, config) {
                    $(self.filtroAtual).slideToggle("slow");
                },
                className: 'btn btn-outline-dark'
            });
        },
        addNewButton: function () {
            let self = this;
            this.addButton('new',
                {
                    text: '<i class="fa fa-file" aria-hidden="true"></i> Novo Registro',
                    action: function (e, dt, node, config) {
                        self.newAction();
                    },
                    className: 'btn btn-outline-dark'
                }
            );
        },
        addButton: function (buttonName, buttonData) {
            let self = this;
            let idx = self.dataTableButtons.length;

            self.buttons[buttonName] = idx;
            self.dataTableButtons[idx] = buttonData;
        }
    }
};