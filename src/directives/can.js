

export default {
    name: "can",
    directive: {
        inserted: function (el, binding) {
            Vue.nextTick(function() {
                let not = false;
                if ( typeof binding.value.not !== "undefined"){
                     if ( binding.value.not === true){
                         not = true;
                     }
                }

                // Cannot
                if ( !not) {
                    if (!Vue.canController.checkPermission(binding.value.model, binding.value.method)) {
                        el.remove();
                    }
                }

                // Can
                if ( not ) {
                    if (Vue.canController.checkPermission(binding.value.model, binding.value.method)) {
                        el.remove();
                    }
                }
            });
        }
    },
    canController: new (function () {
        function CanController() {
                this.data = null;
        }
        CanController.prototype.setData = function (data) {
            if ( typeof data == "string"){
                data = JSON.parse(data);
            }
            this.data = data;
            localStorage.setItem("usu_security", JSON.stringify(data));
        };
        CanController.prototype.checkPermission = function (model, access) {
            let p = false;
            if ( typeof this.data[model] !== "undefined") {
                if (typeof this.data[model][access] !== "undefined") {
                    p = this.data[model][access];
                }
            }

            return p;
        };
        CanController.prototype.can = function (model, access, callback) {
            let self = this;

            if ( Object.keys(this.data).length === 0) {

                this.stack.push(function () {
                    self.can(model, access, callback);
                });
                return null;
            }
            if ( this.checkPermission(model,access)){
                callback();    
            }
        };
        CanController.prototype.canWithBool = function (model, access, callback) {
            callback(this.checkPermission(model,access) );
        };

        CanController.prototype.loadPermission  = function (url, callback) {
            let self = this;
            Vue.Request.get(url, function (resp) {
                self.setData(resp.data);
                callback();
            });
        };
        return CanController;
    }())
}
