import JsonFormatter from 'json-formatter-js';

export default {
    name: "json-content",
    directive: function (el, binding) {
        let renderer = new JsonFormatter(binding.value, 1, {"theme": 'dark'});
        el.innerHTML = "";
        el.appendChild(renderer.render());
    }
}
