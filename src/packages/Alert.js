

export default {
    pack: new (function () {

        function Alert() {
            this.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
        }

        if ( typeof window.toastr == "undefined" ){
            window.toastr = require("toastr");
        }


        Alert.prototype.setOption = function(option, value){
            toastr.options[option] = value;
        };
        Alert.prototype.init =  function(){
            toastr.options = this.options;
        };
        Alert.prototype.info = function (msg, title) {
            toastr.info(msg, title);
        };
        Alert.prototype.success = function (msg, title) {
            toastr.success(msg, title);
        };
        Alert.prototype.warning = function (msg, title) {
            toastr.warning(msg, title);
        };
        Alert.prototype.error = function (msg, title) {
            toastr.error(msg, title);
        };
        Alert.prototype.remove = function (msg, title) {
            toastr.remove();
        };
        Alert.prototype.clear = function (msg, title) {
            toastr.clear();
        };

        return Alert;
    }())
};
