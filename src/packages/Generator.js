export default {
    pack: new (function () {

        function Generator() {
        }

        Generator.prototype.errorsModel = function (data) {
            let model = {};
            for (let j in data) {
                model[j] = {text: ''};
            }

            return model;
        };

        Generator.prototype.reloadValues = function (model) {

            for (let j in model) {
                if (typeof model[j].initial !== "undefined") {
                    model[j].value = model[j].initial;
                } else if (typeof model[j] == "String") {
                    model[j].value = '';
                } else if (typeof model[j] == "Object") {
                    model[j].value = {};
                }
            }

        };

        Generator.prototype.extractValues = function (data) {
            let newData = {};

            for (let j in data) {
                if (typeof data[j].type === "undefined") {
                    data[j].type = null;
                }
                switch (data[j].type) {
                    case "date":
                        newData[j] = Vue.Convert.getDataDB(data[j].value);
                        newData[j] = newData[j] == "Invalid date" ? null : newData[j];
                        break;
                    case "decimal":
                        try {
                            newData[j] = data[j].value.replace(",", ".");
                        } catch (e) {
                            newData[j] = data[j].value;
                        }

                        break;
                    default:
                        newData[j] = data[j].value;
                        break;
                }
            }

            return newData;
        };

        Generator.prototype.getRestData = function (model, response) {

            for (let j in model) {

                if (typeof response.data[j] != "undefined") {
                    switch (model[j].type) {
                        case "date":
                            model[j].value = Vue.Convert.localeFromFormat(response.data[j], "YYYY-MM-DD");
                            model[j].value = model[j].value == "Invalid date" ? null : model[j].value;
                            break;
                        case "datetime":
                            console.log(response.data[j]);
                            model[j].value = Vue.Convert.localeFromFormat(response.data[j], "YYYY-MM-DD HH:II:SS");
                            model[j].value = model[j].value == "Invalid date" ? null : model[j].value;
                            break;
                        default:
                            model[j].value = response.data[j];
                            break;
                    }
                }
            }

            return model;
        };

        Generator.prototype.getRestError = function (response) {

        };


        return Generator;
    }())
};
