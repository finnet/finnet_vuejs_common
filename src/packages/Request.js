export default {
    pack: new (function () {

        function Request() {
        }

        Request.prototype.request = function (type, url, options = {}, onSuccess, onError) {
            let request = {};
            let dataPost = {};
            let headers = {};

            if ( options === null ){
                options = {};
            }

            if ( typeof options['dataPost'] !== "undefined"){
                dataPost = options['dataPost'];
            }

            if ( typeof options['headers'] !== "undefined"){
                headers = options['headers'];
            }

            if ( typeof onSuccess !== "function"){
                onSuccess = function () {};
            }
            if ( typeof onError !== "function"){
                onError = function () {};
            }
            switch (type){
                case 'post':
                case 'patch':
                    request = window.axios[type](url, dataPost,
                        { headers: headers }
                        ).then(onSuccess)
                        .catch(onError);
                    break;
                case 'get':
                case 'delete':
                    request = window.axios[type](url)
                        .then(onSuccess)
                        .catch(onError);
                    break;
            }

        };

        Request.prototype.post = function (url, dataPost, onSuccess, onError, options = {}) {
            options.dataPost = dataPost;
            this.request('post', url, options, onSuccess, onError);
        };

        Request.prototype.patch = function (url, dataPost, onSuccess, onError, options = {}) {
            options.dataPost = dataPost;
            this.request('patch', url, options, onSuccess, onError);
        };

        Request.prototype.get = function (url, onSuccess, onError) {
            this.request('get', url, null, onSuccess, onError);
        };

        Request.prototype.saveModel = function (url, dataModel, errorModel, onSuccess, onError) {

            let dataSend = Vue.Generator.extractValues(dataModel);

            this.post(url, dataSend, onSuccess, function (resp) {
                let data = JSON.parse(resp.request.response);

                if (typeof data.message === "string") {
                    Vue.Alert.error(data.message);
                }
                for (let j in errorModel) {
                    errorModel[j].text = '';
                }

                for (let j in data.errors) {
                    if (typeof errorModel[j] !== "undefined") {
                        errorModel[j].text = data.errors[j];
                    }
                }


                if (typeof onError === "function") {
                    onError(data);
                }
            });
        };

        Request.prototype.updateModel = function (url, dataModel, errorModel, onSuccess, onError) {

            let dataSend = Vue.Generator.extractValues(dataModel);

            this.patch(url, dataSend, onSuccess, function (resp) {
                let data = JSON.parse(resp.request.response);

                if (typeof data.message === "string") {
                    Vue.Alert.error(data.message);
                }
                for (let j in errorModel) {
                    errorModel[j].text = '';
                }

                for (let j in data.errors) {
                    if (typeof errorModel[j] !== "undefined") {
                        errorModel[j].text = data.errors[j];
                    }
                }


                if (typeof onError === "function") {
                    onError(data);
                }
            });
        };

        Request.prototype.loadModel = function (url, dataModel, onSuccess, onError) {


            this.get(url, function (resp) {
                // onSuccess
                let data = JSON.parse(resp.request.response);

                if (typeof data.message === "string") {
                    Vue.Alert.success(data.message);
                }

                for (let j in dataModel) {
                    if (typeof data[j] !== "undefined") {
                        if ( typeof dataModel[j].type === "undefined"){
                            dataModel[j].type = null;
                        }
                        switch (dataModel[j].type){
                            case "date":
                                dataModel[j].value = Vue.Convert.localeFromFormat(data[j], "YYYY-MM-DD");
                                dataModel[j].value = dataModel[j].value == "Invalid date" ? null : dataModel[j].value;
                                break;
                            default:
                                dataModel[j].value = data[j];
                                // newData[j] = data[j].value;
                                // break;
                        }
                    }
                }

                if (typeof onSuccess === "function") {
                    onSuccess(data);
                }

            }, function (resp) {
                // onError
                let data = JSON.parse(resp.request.response);

                if (typeof data.message === "string") {
                    Vue.Alert.error(data.message);
                }


                if (typeof onError === "function") {
                    onError(data);
                }
            });
        };


        return Request;
    }())
};