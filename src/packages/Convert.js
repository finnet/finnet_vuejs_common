

export default {
    pack: new (function () {

        function Convert() {
            this.moment = require('moment');
        }

        Convert.prototype.getDataDB = function(date){
            return this.moment(date, "DD/MM/YYYY").format("YYYY-MM-DD");
        };

        Convert.prototype.fromFormat = function(date){
            return this.moment(date, "DD/MM/YYYY").toDate();
        };

        Convert.prototype.localeFromFormat = function(date, format){
            return (date!==null) ? this.moment(date, format).toDate().toLocaleDateString() : '';
        };

        Convert.prototype.localeDTFromFormat = function(date, format){
            let dateObj = this.moment(date, format).toDate();
            return (date!==null) ? dateObj.toLocaleDateString() + ' ' +  dateObj.toLocaleTimeString() : '';
        };

        return Convert;
    }())
};
